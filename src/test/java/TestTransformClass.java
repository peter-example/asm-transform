
import hk.quantr.asm.transform.ClassPrinter;
import java.io.IOException;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import static org.objectweb.asm.Opcodes.ASM4;

public class TestTransformClass {

	class RemoveDebugAdapter extends ClassVisitor {

		public RemoveDebugAdapter(ClassVisitor cv) {
			super(ASM4, cv);
		}

		@Override
		public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value) {
			// remove all fields
			return null;
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions) {
			// remove all methods
			if (name.equals("getName")) {
				return null;
			} else {
				return super.visitMethod(access, name, descriptor, signature, exceptions);
			}
		}

		@Override
		public void visitEnd() {
			FieldVisitor fv = cv.visitField(Opcodes.ACC_PUBLIC, "hongkong", "I", null, null);
		}

	}

	class MyClasLoader extends ClassLoader {

		public Class defineClass(String name, byte[] b) {
			return defineClass(name, b, 0, b.length);
		}
	}

	@Test
	public void test() throws IOException {
		ClassWriter cw = new ClassWriter(0);
		RemoveDebugAdapter cp = new RemoveDebugAdapter(cw);
		ClassReader cr = new ClassReader("hk.quantr.asm.transform.Class1");
		cr.accept(cp, 0);
		byte[] b2 = cw.toByteArray();

		MyClasLoader myClassLoader = new MyClasLoader();
		Class c = myClassLoader.defineClass("hk.quantr.asm.transform.Class1", b2);

		// print it out
		ClassPrinter classPrinter = new ClassPrinter();
		cr = new ClassReader(b2);
		cr.accept(classPrinter, 0);
	}
}
