
import hk.quantr.asm.transform.ClassPrinter;
import java.io.IOException;
import org.junit.Test;
import org.objectweb.asm.ClassReader;

public class TestPrintClass {

	@Test
	public void test() throws IOException {
		ClassPrinter cp = new ClassPrinter();
		ClassReader cr = new ClassReader("hk.quantr.asm.transform.Class1");
		cr.accept(cp, 0);
	}
}
