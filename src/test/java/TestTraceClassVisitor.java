
import java.io.IOException;
import java.io.PrintWriter;
import org.junit.Test;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.TraceClassVisitor;

public class TestTraceClassVisitor {

	@Test
	public void test() throws IOException {
		//ClassWriter cw=new ClassWriter(0);
		PrintWriter printer=new PrintWriter(System.out,true);
		TraceClassVisitor cp = new TraceClassVisitor(printer);
		ClassReader cr = new ClassReader("hk.quantr.asm.transform.Class1");
		cr.accept(cp, 0);
	}
}
