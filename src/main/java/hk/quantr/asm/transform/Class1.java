package hk.quantr.asm.transform;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Class1 {

	public int x;
	private String name;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
